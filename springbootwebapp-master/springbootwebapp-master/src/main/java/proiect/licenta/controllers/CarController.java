package proiect.licenta.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import proiect.licenta.domain.Automobile;
import proiect.licenta.domain.Car;
import proiect.licenta.services.CarService;

/**
 * Created by ig on 7/29/2017.
 */
@Controller
public class CarController {
    private CarService carService;

    @Autowired
    public void setAutomobileService(CarService carService) {
        this.carService = carService;
    }

    @RequestMapping(value = "/car", method = RequestMethod.GET)
    public String list(Model model){
        Iterable<Car> cars = carService.listAll();
		model.addAttribute("automobiles", cars);
        System.out.println("Returning cars:" + cars.toString());
        return "automobiles";
    }

    @RequestMapping("car/{id}")
    public String get(@PathVariable Integer id, Model model){
        model.addAttribute("automobile", carService.getById(id));
        return "automobileshow";
    }

    @RequestMapping("car/edit/{id}")
    public String edit(@PathVariable Integer id, Model model){
        model.addAttribute("automobile", carService.getById(id));
        return "automobileform";
    }


    @RequestMapping("car/new")
    public String create(Model model){
        model.addAttribute("automobile", new Automobile());
        return "automobileform";
    }


    @RequestMapping(value = "car", method = RequestMethod.POST)
    public String save(Car car){
        carService.save(car);
        return "redirect:/car/" + car.getId();
    }
}
