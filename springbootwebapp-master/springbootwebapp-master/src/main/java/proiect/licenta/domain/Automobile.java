package proiect.licenta.domain;

import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;


@Entity
@Table(indexes = { @Index(name = "IDX_BRAND", columnList = "brand") })
public class Automobile extends Product /*extends DeliverableProduct*/ /* Test: add class in hierarchy */{
	
	// Test: add column in superclass.
//	private String transmission;
	
    private int horsepower;
    
    private String brand;

    public int getHorsepower() {
        return horsepower;
    }

//    public String getTransmission() {
//		return transmission;
//	}
//
//	public void setTransmission(String transmission) {
//		this.transmission = transmission;
//	}

	public void setHorsepower(int horsepower) {
        this.horsepower = horsepower;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Automobile() {
    }

    public Automobile(int horsepower, String brand) {
        this.horsepower = horsepower;
        this.brand = brand;
    }

}
