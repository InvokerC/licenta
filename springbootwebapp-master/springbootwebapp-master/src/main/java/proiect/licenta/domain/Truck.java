package proiect.licenta.domain;

import javax.persistence.Entity;

@Entity
public class Truck extends Automobile {
	
    public Truck(int horsepower, String brand) {
		super(horsepower, brand);
	}
    
    public Truck() {
		super();
	}

	private int axles = 2;

	public int getAxles() {
		return axles;
	}

	public void setAxles(int axles) {
		this.axles = axles;
	}

}
