package proiect.licenta.domain;

import java.math.BigDecimal;

import javax.persistence.Entity;
/* Test: add class in hierarchy */
//@Entity
public class DeliverableProduct extends Product {
	
	private BigDecimal deliveryPrice = new BigDecimal(10);

	public BigDecimal getDeliveryPrice() {
		return deliveryPrice;
	}

	public void setDeliveryPrice(BigDecimal deliveryPrice) {
		this.deliveryPrice = deliveryPrice;
	}
	
}
