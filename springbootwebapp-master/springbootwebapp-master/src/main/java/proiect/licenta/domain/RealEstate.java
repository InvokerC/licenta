package proiect.licenta.domain;

import javax.persistence.Entity;


@Entity
public class RealEstate extends Product {

    private int squareMeters;
    private String address;

    public int getSquareMeters() {
        return squareMeters;
    }

    public void setSquareMeters(int squareMeters) {
        this.squareMeters = squareMeters;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public RealEstate(int squareMeters, String address) {

        this.squareMeters = squareMeters;
        this.address = address;
    }
}
