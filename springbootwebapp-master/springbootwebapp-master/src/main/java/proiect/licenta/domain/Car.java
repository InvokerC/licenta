package proiect.licenta.domain;

import javax.persistence.Entity;

@Entity
public class Car extends Automobile {
    private int doors = 5;

	public Car(int horsepower, String brand) {
		super(horsepower, brand);
	}
	
	public Car() {
		super();
	}

	public int getDoors() {
		return doors;
	}

	public void setDoors(int doors) {
		this.doors = doors;
	}


}