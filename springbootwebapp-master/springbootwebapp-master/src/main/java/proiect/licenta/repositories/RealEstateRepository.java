package proiect.licenta.repositories;


import org.springframework.data.repository.CrudRepository;
import proiect.licenta.domain.RealEstate;

public interface RealEstateRepository extends CrudRepository<RealEstate, Integer> {
}
