package proiect.licenta.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import proiect.licenta.domain.Car;

/**
 * Created by ig on 7/29/2017.
 */
public interface CarRepository extends CrudRepository<Car, Integer> {
	List<Car> findAllByBrandAndDescription(String brand, String description);
	Car findOneByProductId(String productId);
}
