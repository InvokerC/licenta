package proiect.licenta.services;

import proiect.licenta.domain.Car;

public interface CarService {
    Iterable<Car> listAll();

    Car getById(Integer id);

    Car save(Car car);

	void delete(Car car);
}
