package proiect.licenta.services;


import proiect.licenta.domain.Automobile;
import proiect.licenta.domain.RealEstate;

public interface RealEstateService {
    Iterable<RealEstate> listAllRealEstates();

    RealEstate getRealEstatebyId(Integer id);

    RealEstate saveRealEstate(RealEstate realEstate);
}
