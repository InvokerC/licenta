package proiect.licenta.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import proiect.licenta.domain.Car;
import proiect.licenta.repositories.CarRepository;

@Service    
public class CarServiceImpl implements CarService {
    private CarRepository carRepo;

    @Autowired
    public void setAutomobileRepository(CarRepository carRepo) {
        this.carRepo = carRepo;
    }

    @Override
    public Iterable<Car> listAll() {
        return carRepo.findAll();
    }

    @Override
    public Car getById(Integer id) {
        return carRepo.findOne(id);
    }

    @Override
    public Car save(Car car) {
        return carRepo.save(car);
    }
    
    @Override
    public void delete(Car car) {
        carRepo.delete(car);
    }
}
