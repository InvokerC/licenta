package proiect.licenta.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import proiect.licenta.domain.Product;
import proiect.licenta.domain.RealEstate;
import proiect.licenta.repositories.ProductRepository;
import proiect.licenta.repositories.RealEstateRepository;

@Service
public class RealEstateServiceImpl implements RealEstateService{
    private RealEstateRepository realEstateRepository;

    @Autowired
    public void setRealEstateRepository(RealEstateRepository realEstateRepository) {
        this.realEstateRepository= realEstateRepository;
    }

    @Override
    public Iterable<RealEstate> listAllRealEstates() {
        return realEstateRepository.findAll();
    }

    @Override
    public RealEstate getRealEstatebyId(Integer id) {
        return realEstateRepository.findOne(id);
    }

    @Override
    public RealEstate saveRealEstate(RealEstate product) {
        return realEstateRepository.save(product);
    }
}
