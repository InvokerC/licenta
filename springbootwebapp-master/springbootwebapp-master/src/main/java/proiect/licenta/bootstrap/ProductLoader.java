package proiect.licenta.bootstrap;

import proiect.licenta.domain.Automobile;
import proiect.licenta.domain.Car;
import proiect.licenta.domain.Product;
import proiect.licenta.repositories.ProductRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class ProductLoader implements ApplicationListener<ContextRefreshedEvent> {

	@Autowired
    private ProductRepository productRepository;

    private Logger log = Logger.getLogger(ProductLoader.class);

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {

        Product automobile = new Car();
        automobile.setDescription("Dacia");
        automobile.setPrice(new BigDecimal("30000"));
        automobile.setImageUrl("www.testUrl.com");
        automobile.setProductId("235268845711068308");
        productRepository.save(automobile);

        log.info("Saved automobile - id: " + automobile.getId());

        Product automobile1 = new Car();
        automobile1.setDescription("Mercedes");
        automobile1.setImageUrl("www.testUrl2.com");
        automobile1.setProductId("168639393495335947");
        automobile1.setPrice(new BigDecimal("210000"));
        productRepository.save(automobile1);

        log.info("Saved automobile - id:" + automobile1.getId());

        log.info("Saved Mug - id:" + automobile.getId());

    }
}
