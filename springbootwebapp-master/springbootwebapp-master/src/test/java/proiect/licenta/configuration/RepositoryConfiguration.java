package proiect.licenta.configuration;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableAutoConfiguration
@EntityScan(basePackages = {"proiect.licenta"})
@EnableJpaRepositories(basePackages = {"proiect.licenta.repositories"})
@EnableTransactionManagement
public class RepositoryConfiguration {
}
