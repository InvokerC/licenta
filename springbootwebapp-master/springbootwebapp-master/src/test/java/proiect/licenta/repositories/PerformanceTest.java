package proiect.licenta.repositories;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

import com.codahale.metrics.CsvReporter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.codahale.metrics.Timer.Context;

import proiect.licenta.SpringBootWebApplication;
import proiect.licenta.domain.Car;
import proiect.licenta.services.CarServiceImpl;

//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest(classes = {RepositoryConfiguration.class})
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = SpringBootWebApplication.class)
public class PerformanceTest {

	private static final int BULK_SIZE = 1000;
	private static final int NUM_BULKS = 5;
	
	final MetricRegistry metrics = new MetricRegistry();
	private final Timer insertions = metrics.timer("Insertion of bulk " + BULK_SIZE);
	private final Timer updates = metrics.timer("Update of bulk " + BULK_SIZE);
	private final Timer deletes = metrics.timer("Deletions of bulk " + 1);
	private final Timer queries = metrics.timer("Queries of bulk " + BULK_SIZE);
	
	private final CsvReporter reporter = CsvReporter.forRegistry(metrics)
            .formatFor(Locale.US)
            .convertRatesTo(TimeUnit.SECONDS)
            .convertDurationsTo(TimeUnit.MILLISECONDS)
            .build(new File("C:/data/"));
	
	@Autowired
	private CarServiceImpl carService;
	
	@Autowired
	private CarRepository carRepository;
	
	@Before
	public void setup() {
		carRepository.deleteAll();
	}
	
	@After
	public void tearDown() {
//		carRepository.deleteAll();
	}
	
	@Test
	public void testOnOneRecord() {
		carService.save(createCar("00"));
		
		Car existingCar = carRepository.findOneByProductId("00");
//		Car carToUpdate = createCar("00");
//		carToUpdate.setId(existingCar.getId());
		existingCar.setBrand("B");
		carService.save(existingCar);
		
		List<Car> found = carRepository.findAllByBrandAndDescription("B", "Description00");
		assertEquals(1, found.size());
	}
	
	@Test
	public void testBulkCRUD() {
		insertBulk("warmup");
		reporter.start(1, TimeUnit.SECONDS);
		for (int i = 0 ; i < NUM_BULKS ; i++) {
			// Create
			Context context = insertions.time();
			insertBulk(String.valueOf(i));
			context.stop();
		}
		for (int i = 0 ; i < NUM_BULKS ; i++) {
			// Update
			List<Car> carsToUpdate = new ArrayList<>();
			for (int j = 0 ; j < BULK_SIZE ; j++) {
				Car existingCar = carRepository.findOneByProductId(i + String.valueOf(j));
				existingCar.setHorsepower(200);
				carsToUpdate.add(existingCar);
			}
			Context context = updates.time();
			for (Car carToUpdate : carsToUpdate) {
				carService.save(carToUpdate);
			}
			context.stop();
		}
		for (int i = 0 ; i < NUM_BULKS ; i++) {
			// Query by brand + description
			Context context = queries.time();
			List<Car> found = carRepository.findAllByBrandAndDescription("Brand" + i + i, "Description" + i + i);
			context.stop();
			// Delete those found
			context = deletes.time();
			for (Car toDelete : found) {
				carRepository.delete(toDelete);
			}
			context.stop();
		}
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// Ignore.
		}
		reporter.stop();
		
	}
	
	private void insertBulk(String suffix) {
		for (int i = 0 ; i < BULK_SIZE ; i++) {
			carService.save(createCar(suffix+String.valueOf(i)));
		}
	}

	private Car createCar(String suffix) {
		Car result = new Car();
		result.setBrand("Brand" + suffix);
		result.setDescription("Description" + suffix);
		result.setHorsepower(177);
		result.setProductId("" + suffix);
		result.setPrice(new BigDecimal(100000));
		return result;
	}
	
	

}
