-- Join
-- Test: add column in superclass
ALTER TABLE automobile ADD COLUMN transmission CHARACTER VARYING(255);
-- Test: drop column in superclass
ALTER TABLE automobile DROP COLUMN transmission;
-- Test: add superclass
CREATE TABLE IF NOT EXISTS deliverable_product (     
	id integer NOT NULL,
	delivery_price numeric(19, 2),
    CONSTRAINT deliverable_product_pkey PRIMARY KEY (id),
    CONSTRAINT deliverable_product_fkey FOREIGN KEY (id)
        REFERENCES product (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
	);	
-- Also need to insert IDs from subclasses in this table for JOIN.	
-- Test: remove superclass
DROP TABLE deliverable_product;

-- Table Per Class
-- Test: add column in superclass
ALTER TABLE automobile ADD COLUMN transmission CHARACTER VARYING(255);
ALTER TABLE car ADD COLUMN transmission CHARACTER VARYING(255);
ALTER TABLE truck ADD COLUMN transmission CHARACTER VARYING(255);
-- Test: drop column in superclass
ALTER TABLE automobile DROP COLUMN transmission;
ALTER TABLE car DROP COLUMN transmission
ALTER TABLE truck DROP COLUMN transmission;
-- Test: add superclass
CREATE TABLE IF NOT EXISTS deliverable_product (     
	id integer NOT NULL,
	delivery_price numeric(19, 2),
    CONSTRAINT deliverable_product_pkey PRIMARY KEY (id),
    CONSTRAINT deliverable_product_fkey FOREIGN KEY (id)
        REFERENCES product (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
	);
ALTER TABLE automobile ADD COLUMN delivery_price numeric(19, 2);
ALTER TABLE car ADD COLUMN delivery_price numeric(19, 2);
ALTER TABLE truck ADD COLUMN delivery_price numeric(19, 2);
-- Test: remove superclass
DROP TABLE deliverable_product;
ALTER TABLE automobile DROP COLUMN delivery_price;
ALTER TABLE car DROP COLUMN delivery_price;
ALTER TABLE truck DROP COLUMN delivery_price;

-- Single Table
-- Test: add column in superclass
ALTER TABLE product ADD COLUMN transmission CHARACTER VARYING(255);
-- Test: drop column in superclass
ALTER TABLE product DROP COLUMN transmission;
-- Test: add superclass
ALTER TABLE product ADD COLUMN delivery_price numeric(19, 2);
-- Test: remove superclass
ALTER TABLE product DROP COLUMN delivery_price;